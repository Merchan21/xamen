package facci.mieles.examen;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import facci.mieles.examen.Model.MarketAdapter;
import facci.mieles.examen.Model.modelo;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtraActivity extends AppCompatActivity {
    RecyclerView recyclerView1;
    ArrayList<modelo> lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otra);


        lista = new ArrayList<>();
        recyclerView1 = (RecyclerView)findViewById(R.id.recyclerView1);
        recyclerView1.setLayoutManager(new LinearLayoutManager(this));
        mostrar();

    }

    private void mostrar() {

        final MarketAdapter adapter = new MarketAdapter();
        Call<List<modelo>> call = adapter.getPost();
        call.enqueue(new Callback<List<modelo>>() {
            @Override
            public void onResponse(Call<List<modelo>> call, Response<List<modelo>> response) {
                List<modelo> lista = response.body();
                for (modelo post: lista
                ) {
                    lista.add(post);
                }

            }

            @Override
            public void onFailure(Call<List<modelo>> call, Throwable t) {

            }
        });

    }
}
