package facci.mieles.examen.Model;

public class modelo {

    private String cedula, ciudad, pais, descripcion, nombre, apellido, estado;

    public modelo(String cedula, String ciudad, String pais, String descripcion, String nombre, String apellido, String estado) {
        this.cedula = cedula;
        this.ciudad = ciudad;
        this.pais = pais;
        this.descripcion = descripcion;
        this.nombre = nombre;
        this.apellido = apellido;
        this.estado = estado;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
