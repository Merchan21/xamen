package facci.mieles.examen.Model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

interface MarketService {

    @POST(ApiConstants.MARKET_POST_ENDPOINT)
    Call<modelo> InsertPost(@Body modelo post);

    @GET(ApiConstants.MARKET_POST_ENDPOINT)
    Call<List<modelo>> getPost();
}
