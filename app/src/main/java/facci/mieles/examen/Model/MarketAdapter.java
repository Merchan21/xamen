package facci.mieles.examen.Model;


import java.util.List;

import retrofit2.Call;

public class MarketAdapter extends BaseAdapter implements MarketService{
    private MarketService marketService;

    public MarketAdapter(){
        super(ApiConstants.BASE_POST_URL);
        marketService=creativeService(MarketService.class);
    }
    @Override
    public Call<modelo> InsertPost(modelo post) {
        return marketService.InsertPost(post);
    }
    @Override
    public Call<List<modelo>> getPost() {
        return marketService.getPost();
    }

}


