package facci.mieles.examen.Model;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import facci.mieles.examen.R;

public class Adaptador_materias extends RecyclerView.Adapter<Adaptador_materias.ViewHolderImagenes> {

    ArrayList<modelo> listaImagenes;

    public Adaptador_materias(ArrayList<modelo> listaImagenes) {
        this.listaImagenes = listaImagenes;
    }

    @NonNull
    @Override
    public Adaptador_materias.ViewHolderImagenes onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);
        return new ViewHolderImagenes(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Adaptador_materias.ViewHolderImagenes holder, int position) {




    }

    @Override
    public int getItemCount() {
        return listaImagenes.size();
    }

    public class ViewHolderImagenes extends RecyclerView.ViewHolder {

        TextView nombre, cedula, estado;

        public ViewHolderImagenes(@NonNull View itemView) {
            super(itemView);

            cedula = (TextView) itemView.findViewById(R.id.cedula);
            nombre = (TextView) itemView.findViewById(R.id.pais);
            estado = (TextView) itemView.findViewById(R.id.estado);
        }
    }
}